from unittest import TestCase

from src.enums.autoEnum import enum


class TestEnum(TestCase):

    #  Enumerate a class with integer annotations
    def test_enumerate_class_with_integer_annotations(self):
        @enum
        class MyClass:
            a: int
            b: int
            c: int

        self.assertEqual(MyClass.a.id, 0)
        self.assertEqual(MyClass.a.name, 'a')
        self.assertEqual(MyClass.a.value, 0)

        self.assertEqual(MyClass.b.id, 1)
        self.assertEqual(MyClass.b.name, 'b')
        self.assertEqual(MyClass.b.value, 1)

        self.assertEqual(MyClass.c.id, 2)
        self.assertEqual(MyClass.c.name, 'c')
        self.assertEqual(MyClass.c.value, 2)

    #  Enumerate a class with string annotations
    def test_enumerate_class_with_string_annotations(self):
        @enum
        class MyClass:
            a: str
            b: str
            c: str

        self.assertEqual(MyClass.a.id, 0)
        self.assertEqual(MyClass.a.name, 'a')
        self.assertEqual(MyClass.a.value, 'a')

        self.assertEqual(MyClass.b.id, 1)
        self.assertEqual(MyClass.b.name, 'b')
        self.assertEqual(MyClass.b.value, 'b')

        self.assertEqual(MyClass.c.id, 2)
        self.assertEqual(MyClass.c.name, 'c')
        self.assertEqual(MyClass.c.value, 'c')

    #  Enumerate a class with mixed annotations
    def test_enumerate_class_with_mixed_annotations(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: int
                b: str
                c: int

    #  Enumerate a class with no annotations
    def test_enumerate_class_with_no_annotations(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                pass

    #  Enumerate a class with one annotation
    def test_enumerate_class_with_one_annotation(self):
        @enum
        class MyClass:
            a: int

        self.assertEqual(MyClass.a.id, 0)
        self.assertEqual(MyClass.a.name, 'a')
        self.assertEqual(MyClass.a.value, 0)

    #  Enumerate a class with multiple annotations of the same type
    def test_enumerate_class_with_multiple_annotations_same_type(self):
        @enum
        class MyClass:
            a: int
            b: int
            c: int

        self.assertEqual(MyClass.a.id, 0)
        self.assertEqual(MyClass.a.name, 'a')
        self.assertEqual(MyClass.a.value, 0)

        self.assertEqual(MyClass.b.id, 1)
        self.assertEqual(MyClass.b.name, 'b')
        self.assertEqual(MyClass.b.value, 1)

        self.assertEqual(MyClass.c.id, 2)
        self.assertEqual(MyClass.c.name, 'c')
        self.assertEqual(MyClass.c.value, 2)

    #  Enumerate a class with a single annotation of unsupported type
    def test_enumerate_class_with_single_annotation_unsupported_type(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: float

    #  Enumerate a class with multiple annotations of unsupported types
    def test_enumerate_class_with_multiple_annotations_unsupported_types(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: float
                b: bool
                c: list

    #  Enumerate a class with annotations of different types and default values
    def test_enumerate_class_with_annotations_different_types_default_values(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: int = 10
                b: str = 'default'
                c: bool = True


    #  Enumerate a class with overridden values of unsupported type
    def test_enumerate_class_with_overridden_values_unsupported_type(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: int = 10
                b: str = 'default'
                c: bool = True

                a = 20
                b = 'overridden'
                c = False

    #  Enumerate a class with overridden values of different type
    def test_enumerate_class_with_overridden_values_different_type(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: int = 10
                b: str = 'default'
                c: bool = True

                a = 'overridden'
                b = 20
                c = 'False'

    #  Enumerate a class with multiple annotations of different types
    def test_enumerate_class_with_multiple_annotations_different_types(self):
        with self.assertRaises(ValueError):
            @enum
            class MyClass:
                a: int
                b: str
                c: bool
