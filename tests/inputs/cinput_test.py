from unittest import TestCase

from src.inputs import cinput


class TestCinput(TestCase):

    #  Function prompts user for input and returns user input as string
    def test_prompt_user_input_as_string(self):
        result = cinput('', default=42, data_type=str)
        self.assertEqual(result, "42")

    #  Function can take a default value and return it if user input is empty
    def test_default_value_if_user_input_empty(self):
        result = cinput("", default=42)
        self.assertEqual(result, 42)

    #  Function can take a data_type parameter and cast user input to the specified data type
    def test_cast_user_input_to_specified_data_type(self):
        result = cinput("Enter a number: ", data_type=float)
        self.assertEqual(result, 3.14)

    #  Function can take an autocast parameter and attempt to cast user input to the appropriate data type
    def test_attempt_to_cast_user_input_to_appropriate_data_type(self):
        result = cinput("Enter a number: ", autocast=True)
        self.assertEqual(result, 3.14)

    #  Function can handle input of type list and cast it to a list
    def test_handle_input_of_type_list_and_cast_to_list(self):
        result = cinput("Enter a list: ", data_type=list)
        self.assertEqual(result, [1, 2, 3])

    #  Function can handle input of type tuple and cast it to a tuple
    def test_handle_input_of_type_tuple_and_cast_to_tuple(self):
        result = cinput("Enter a tuple: ", data_type=tuple)
        self.assertEqual(result, (1, 2, 3))

    #  Function raises an error if data_type is specified but user input cannot be cast to that type
    def test_raise_error_if_user_input_cannot_be_cast_to_specified_data_type(self):
        with self.assertRaises(ValueError):
            result = cinput("Enter a number: ", data_type=int)

    #  Function raises an error if autocast is specified but user input cannot be cast to any of the specified types
    def test_raise_error_if_user_input_cannot_be_cast_to_any_specified_types(self):
        with self.assertRaises(ValueError):
            result = cinput("Enter a number: ", autocast=True)


    #  Function raises an error if input of type list, tuple, or dict is not formatted correctly
    def test_raise_error_if_input_of_type_list_tuple_dict_not_formatted_correctly(self):
        with self.assertRaises(SyntaxError):
            result = cinput("Enter a list: ", data_type=list)

    #  Function can handle input of type bool and cast it to a boolean value
    def test_handle_input_of_type_bool_and_cast_to_boolean_value(self):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout, \
                patch('sys.stdin', new_callable=StringIO, side_effect=['True\n']):
            result = cinput("Enter a boolean value: ", data_type=bool)

        expected_output = "Enter a boolean value: "
        self.assertEqual(mock_stdout.getvalue(), expected_output)
        self.assertEqual(result, True)

    #  Function can handle input of type float and cast it to a float value
    def test_handle_input_of_type_float_and_cast_to_float_value(self):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout, \
                patch('sys.stdin', new_callable=StringIO, side_effect=['3.14\n']):
            result = cinput("Enter a float value: ", data_type=float)

        expected_output = "Enter a float value: "
        self.assertEqual(mock_stdout.getvalue(), expected_output)
        self.assertEqual(result, 3.14)

    #  Function can handle input of type int and cast it to an integer value
    def test_handle_input_of_type_int_and_cast_to_integer_value(self):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout, \
                patch('sys.stdin', new_callable=StringIO, side_effect=['42\n']):
            result = cinput("Enter an integer value: ", data_type=int)

        expected_output = "Enter an integer value: "
        self.assertEqual(mock_stdout.getvalue(), expected_output)
        self.assertEqual(result, 42)
