from unittest import TestCase
from src.maths import sum, sub, prom


class TestSum(TestCase):

    #  Should return 0 when no arguments are passed
    def test_no_arguments_passed(self):
        result = sum()
        self.assertEqual(result, 0)

    #  Should return the sum of two integers
    def test_sum_of_two_integers(self):
        result = sum(2, 3)
        self.assertEqual(result, 5)

    #  Should return the sum of two floats
    def test_sum_of_two_floats(self):
        result = sum(2.5, 3.7)
        self.assertEqual(result, 6.2)

    #  Should return the sum of multiple integers
    def test_sum_of_multiple_integers(self):
        result = sum(1, 2, 3, 4, 5)
        self.assertEqual(result, 15)

    #  Should return the sum of multiple floats
    def test_sum_of_multiple_floats(self):
        result = sum(1.5, 2.3, 3.7, 4.2)
        self.assertEqual(result, 11.7)

    #  Should return the sum of a list of integers
    def test_sum_of_list_of_integers(self):
        result = sum([1, 2, 3, 4, 5])
        self.assertEqual(result, 15)

    #  Should return the sum of a very large number of integers
    def test_sum_of_very_large_number_of_integers(self):
        result = sum(*range(1000000))
        self.assertEqual(result, 499999500000)

    #  Should return the sum of a very large number of floats
    def test_sum_of_very_large_number_of_floats(self):
        result = sum([0.1] * 1000001)
        self.assertAlmostEqual(int(result), 100000)

    #  Should return the sum of a list of length 0
    def test_sum_of_empty_list(self):
        result = sum([])
        self.assertEqual(result, 0)

    #  Should return the sum of a tuple of length 1
    def test_sum_of_tuple_length_1(self):
        result = sum((5,))
        self.assertEqual(result, 5)

    #  Should return the sum of a nested list of length 0
    def test_sum_of_nested_list_length_0(self):
        result = sum([[]])
        self.assertEqual(result, 0)

    #  Should return the sum of a tuple of floats
    def test_sum_of_tuple_of_floats(self):
        result = sum((1.5, 2.3, 3.7))
        self.assertEqual(result, 7.5)


class TestSub(TestCase):

    #  Can sum two integers
    def test_sum_two_integers(self):
        result = sub(2, 3)
        self.assertEqual(result, -1)

    #  Can sum multiple integers
    def test_sum_multiple_integers(self):
        result = sub(2, 3, 4, 5)
        self.assertEqual(result, -10)

    #  Can sum integers and floats
    def test_sum_integers_and_floats(self):
        result = sub(2, 3.5, 4)
        self.assertEqual(result, -5.5)

    #  Can sum nested lists and tuples of integers and floats
    def test_sum_nested_lists_and_tuples_of_numbers(self):
        with self.assertRaises(ValueError):
            result = sub([1, 2, 3], (4, 5.5))

    #  Can sum nested lists and tuples of mixed types, ignoring non-numeric elements
    def test_sum_nested_lists_and_tuples_of_mixed_types(self):
        with self.assertRaises(ValueError):
            result = sub([1, 'a', 2, 'b'], (3, 'c', 4.5))

    #  Can sum empty list or tuple, returning 0
    def test_sum_empty_list_or_tuple(self):
        with self.assertRaises(ValueError):
            result = sub([], ())

    #  Can sum list or tuple with only non-numeric elements, returning 0
    def test_sum_list_or_tuple_with_non_numeric_elements(self):
        with self.assertRaises(ValueError):
            result = sub(['a', 'b', 'c'], ('d', 'e'))

    #  Can raise ValueError if errors=True and non-numeric element is found
    def test_raise_value_error_if_errors_true_and_non_numeric_element_found(self):
        with self.assertRaises(ValueError):
            sub(1, 2, 'a', errors=True)

    #  Can sum negative integers and floats
    def test_sum_negative_numbers(self):
        result = sub(-2, -3.5, -4)
        self.assertEqual(result, 5.5)

    #  Can sum large integers and floats without overflow
    def test_sum_large_numbers_without_overflow(self):
        result = sub(
            1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000,
            2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000)
        self.assertEqual(
            result,
            -1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000)

    #  Can sum a mix of positive and negative numbers
    def test_sum_mix_of_positive_and_negative_numbers(self):
        result = sub(2, -3, 4, -5)
        self.assertEqual(result, 6)

    #  Can sum a mix of large and small numbers without overflow
    def test_sum_mix_of_large_and_small_numbers_without_overflow(self):
        result = sub(1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000,
                     -2000000000000000000000000000000000000000000000000000000)
        self.assertEqual(result,
                         1000000000000000000000000000000000000002000000000000000000000000000000000000000000000000000000)


class TestProm(TestCase):

    #  Calculates the average of a list of integers
    def test_average_of_integers(self):
        result = prom(1, 2, 3, 4, 5)
        self.assertEqual(result, 3.0)

    #  Calculates the average of a list of floats
    def test_average_of_floats(self):
        result = prom(1.5, 2.5, 3.5, 4.5, 5.5)
        self.assertEqual(result, 3.5)

    #  Calculates the average of a combination of integers and floats
    def test_average_of_mixed_numbers(self):
        result = prom(1, 2.5, 3, 4.5, 5)
        self.assertEqual(result, 3.2)

    #  Calculates the average of a list of tuples containing integers and/or floats
    def test_average_of_tuples(self):
        result = prom((1, 2), (3.5, 4.5), (5, 6))
        self.assertEqual(result, 3.67)

    #  Calculates the average of a list of lists containing integers and/or floats
    def test_average_of_lists(self):
        result = prom([1, 2], [3.5, 4.5], [5, 6])
        self.assertEqual(result, 3.67)

    #  Rounds the result to a specified number of decimal places
    def test_rounding_decimal_places(self):
        result = prom(1, 2, 3, 4, 5, decimal=1)
        self.assertEqual(result, 3.0)

    #  Returns 0 when no arguments are passed
    def test_no_arguments_passed(self):
        with self.assertRaises(ZeroDivisionError):
            result = prom()

    #  Raises a TypeError when a non-numeric argument is passed
    def test_non_numeric_argument(self):
        with self.assertRaises(ValueError):
            prom(1, 'a', 3, errors=True)

    #  Returns the correct result when only one argument is passed
    def test_single_argument(self):
        result = prom(5)
        self.assertEqual(result, 5)

    #  Returns the correct result when a list containing only one integer is passed
    def test_list_with_single_integer(self):
        result = prom([5])
        self.assertEqual(result, 5)

    #  Returns the correct result when a list containing only one float is passed
    def test_list_with_single_float(self):
        result = prom([5.5])
        self.assertEqual(result, 5.5)

    #  Returns the correct result when a tuple containing only one integer is passed
    def test_tuple_with_single_integer(self):
        result = prom((5,))
        self.assertEqual(result, 5)
