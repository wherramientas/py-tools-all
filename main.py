from itertools import chain
from random import choice, choices

from enum import Enum

from inputs.cinput import cinput

if __name__ == '__main__':

    class Test(Enum):
        default = 0
        cast = 1
        autocast = 2
        all = 3
        stop = 4


    while True:
        data = {'int': ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1000000", "115216516546",
                        "65469416498165984"],
                'float': ["1.1", "2.5", "521964.66", "2.", "0.2256", "41954.", ".25156", "65461.56465", "54.5644356"],
                'bool': ["True", "true", "1", "0", "false", "False"],
                'tuple': ["(1,2,3)", "(1,{2},3)", "(1,[2],3)", "('hola',2,3)", "(1,2.2,3)", "(1,'2',3)",
                          "('1','2','3')"],
                'list': ["[1,2,3]", "['1','datos']", "[1,5,6,8,1.2,3.2,'parrafo']", "[(1,2),(3,4)]",
                         "[{'text':{1,2,3,4}}]"],
                'dict': ["{1:'un dato'}", "{2:['varios','datos']}", "{3:{'set',('tuplas',[1,2,3])}}",
                         "{4:[{5:'datos'}]}"],
                'set': ["{1,2,3}", "{1,2,3}", "{1,2,3}", "{'frase',2,3}", "{1,2.3,3}", "{1,'2',3}", "{'1','2','3'}"],
                'error': ["{", "}", "{1,2,3,4,5", "5,4,3,2,1}", "{1 2 3 4 5 6}", "{'hola mundo}", "{mundo hola'}",
                          "1,,2", "2,152,0", "-15.,5", "(1,5,2,4,5", ",9,85,5,6,2,5,'hola')",
                          "['hola mundo'", "'texto']", ""]
                }

        datall = [item for valor in data.values() for item in (valor if isinstance(valor, list) else [valor])]

        test = Test.cast

        if test == Test.autocast or test == Test.all:
            print('*' * 100)
            result = cinput('', default=choices(datall), autocast=True)
            print(result, type(result))
        if test == Test.cast or test == Test.all:
            print('*' * 100)
            result = cinput('', default=choices(data['int']), data_type=int)
            result = cinput('', default=choices(data['error']), data_type=int)
            print(result, type(result))
            print('*' * 100)
            result = cinput('', default=choices(data['float']), data_type=float)
            result = cinput('', default=choices(data['error']), data_type=float)
            print(result, type(result))
            print('*' * 100)
            result = cinput('', default=choices(data['bool']), data_type=bool)
            result = cinput('', default=choices(data['error']), data_type=bool)
            print(result, type(result))
            print('*' * 100)
            result = cinput('', default=choices(data['tuple']), data_type=tuple)
            result = cinput('', default=choices(data['error']), data_type=tuple)
            print(result, type(result))
            print('*' * 100)
            result = cinput('', default=choices(data['list']), data_type=list)
            result = cinput('', default=choices(data['error']), data_type=list)
            print(result, type(result))
            print('*' * 100)
            result = cinput('', default=choices(data['dict']), data_type=dict)
            result = cinput('', default=choices(data['error']), data_type=dict)
            print(result, type(result))
            print('*' * 100)
            result = cinput('', default=choices(data['set']), data_type=set)
            result = cinput('', default=choices(data['error']), data_type=set)
            print(result, type(result))
        if test == Test.default or test == Test.all:
            print('*' * 100)
            result = cinput('', default=choices(datall))
            print(result, type(result))

        input("Press Enter:")
