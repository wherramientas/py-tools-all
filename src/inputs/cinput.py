import re
import sys

__all__ = ['cinput']

from typing import Any

from src.inputs.cast import _autocast, str_to_dict_set


def cinput(prompt, default: Any = None, end=': ', data_type=None, autocast=False) -> Any:
    """
    Función que muestra un prompt personalizado y lee una línea de la entrada estándar.

    :param prompt: Mensaje que se muestra como prompt.
    :param default: Valor por default / evita el ingreso manual
    :param end: Sufijo que se muestra al final del prompt.
    :param data_type: Tipo de dato esperado para la entrada.
    :param autocast: Si se debe intentar realizar una conversión automática del tipo de dato.
    :return: La línea ingresada por el usuario, sin el carácter de nueva línea al final.
    :rtype: str or Any
    """
    if default is None:
        sys.stdout.write(prompt + end)
        capture = sys.stdin.readline().strip()
    else:
        capture = default

    print(data_type is None, not autocast)
    if data_type is None and not autocast:
        return capture

    if autocast:
        return _autocast(capture)

    try:
        if data_type is tuple:
            limpiar = re.sub(r"[(|)|\"|\']", "", capture)
            div = limpiar.split(',')
            return tuple([_autocast(item.strip()) for item in div])
        if data_type is list:
            limpiar = re.sub(r"[\[|\]|\"|\']", "", capture)
            div = limpiar.split(',')
            return [_autocast(item.strip()) for item in div]
        if data_type is dict:
            return str_to_dict_set(capture)
        return data_type(capture)
    except Exception as e:
        print(f"Error {e.args[0]} al cast {data_type.__name__}. Se devolverá como cadena.")
    return capture
