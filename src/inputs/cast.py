import re

# __all__ = ['str_to_tuple', 'str_to_list', 'str_to_dict_set', '_autocast']
__all__ = [
    '_autocast'
]

from typing import Dict, Any, Set, List, Tuple


def str_to_dict_set(capture: str) -> Dict[Any, Any] or Set[Any]:
    """
        Convierte una cadena representando un diccionario o conjunto a su tipo correspondiente.

        :param capture: Cadena que representa un diccionario o conjunto.
        :return: Diccionario o conjunto obtenido de la cadena.
        :rtype: dict or set
    """
    convert = eval(capture)
    if isinstance(convert, dict or set):
        return convert


def str_to_list(capture: str) -> List[Any]:
    """
        Convierte una cadena representando una lista a su tipo correspondiente.

        :param capture: Cadena que representa una lista.
        :return: Lista obtenida de la cadena.
        :rtype: list
    """
    convert = eval(capture)
    if isinstance(convert, list):
        return convert


def str_to_tuple(capture: str) -> Tuple:
    """
        Convierte una cadena representando una tupla a su tipo correspondiente.

        :param capture: Cadena que representa una tupla.
        :return: Tupla obtenida de la cadena.
        :rtype: tuple
    """
    convert = eval(capture)
    if isinstance(convert, tuple):
        return convert


def _autocast(block: str, cast: List[type] = None) -> Any:
    """
        Intenta convertir un bloque al tipo especificado en la lista de cast.

        :param block: Bloque a convertir.
        :param cast: Lista de tipos a probar en orden.
        :return: Bloque convertido al primer tipo válido en la lista de cast.
        :rtype: Any
    """
    if cast is None:
        cast = [str, int, float, bool, list, tuple, dict, set]
    try:
        # Intentar convertir el bloque al tipo actual en la lista
        if cast[-1] in [set, dict, list, tuple]:
            if '(' in block and ')' in block and cast[-1] in [tuple]:
                try:
                    return eval(block)
                except SyntaxError:
                    raise ValueError()
            if '[' in block and ']' in block and cast[-1] in [list]:
                try:
                    limpiar = re.sub(r"[\[|\]|\"|\']", "", block)
                    div = limpiar.split(',')
                    return [_autocast(item.strip()) for item in div]
                except SyntaxError:
                    raise SyntaxError()
            if '{' in block and '}' in block and cast[-1] in [dict, set]:
                try:
                    return str_to_dict_set(block)
                except Exception:
                    raise SyntaxError()
        elif cast[-1] is bool:
            # Si el tipo es bool, manejar casos especiales para cadenas que representan valores booleanos
            lower_block = block.lower()
            if lower_block == 'true' or lower_block == '1':
                return True
            elif lower_block == 'false' or lower_block == '0':
                return False
            else:
                # Si no es 'true' o 'false', dejar que el bloque sea procesado normalmente
                raise ValueError
        else:
            casted_block = cast[-1](block)
            return casted_block

        return _autocast(block, cast[:-1]) if len(cast) > 1 else None

    except (ValueError, TypeError):
        # Si la conversión falla, intentar con los tipos restantes en la lista
        if len(cast) > 1:
            return _autocast(block, cast[:-1])
        else:
            # Si no hay más tipos para probar, devolver el bloque original
            return block
