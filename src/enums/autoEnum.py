__all__ = ('enum', 'Enum')


class ItemEnum:
    """
    Clase que representa un elemento enumerado con propiedades id, name y value.

    Attributes:
        id (int): Identificador único del elemento.
        name (str): Nombre del elemento.
        value (object): Valor asociado al elemento.
    """

    def __init__(self, dni, name, value):
        """
        Inicializa una instancia de ItemEnum.

        Args:
            dni (int): Identificador único del elemento.
            name (str): Nombre del elemento.
            value (object): Valor asociado al elemento.
        """
        self.id = dni
        self.name = name
        self.value = value

    def __str__(self):
        """
        Representación en cadena del objeto.

        Returns:
            str: Cadena que representa el identificador del elemento.
        """
        return f'{self.id}'


################################################################################################################

def enum(cls):
    """
    Decorador que transforma una clase anotada en una enumeración utilizando la clase ItemEnum.

    Args:
        cls: Clase a decorar.

    Returns:
        cls: Clase decorada como enumeración.
    """
    # Verificar que todas las anotaciones son del mismo tipo
    tipos = set(cls.__annotations__.values())

    if len(tipos) != 1:
        raise ValueError("Todas las anotaciones deben ser del mismo tipo")

    tipo = tipos.pop()
    i = 0

    # Inicializar las variables según el tipo
    for nombre, valor in cls.__annotations__.items():
        if tipo is int:
            setattr(cls, nombre, ItemEnum(dni=i, name=nombre, value=getattr(cls, nombre, i)))
        elif tipo is str:
            setattr(cls, nombre, ItemEnum(dni=i, name=nombre, value=getattr(cls, nombre, nombre)))
        else:
            raise ValueError("Tipo no soportado")
        i += 1

    return cls


################################################################################################################
class EnumMeta(type):
    def __new__(cls, name, bases, dct):
        new_cls = super().__new__(cls, name, bases, dct)
        new_cls._values = []

        i = 0

        if name != 'Enum':
            for k, v in dct.items():
                if k == '__annotations__':
                    tipos = set(v.values())

                    if len(tipos) != 1:
                        raise ValueError("Todas las anotaciones deben ser del mismo tipo")

                    tipo = tipos.pop()

                    for var, inst in v.items():
                        if tipo is int:
                            new_cls._values.append(inst)
                            setattr(cls, var, ItemEnum(dni=i, name=var, value=getattr(cls, var, i)))
                        elif tipo is str:
                            new_cls._values.append(inst)
                            setattr(cls, var, ItemEnum(dni=i, name=var, value=getattr(cls, var, var)))
                        else:
                            raise ValueError("Tipo no soportado")
                        i += 1
        return new_cls


class Enum(metaclass=EnumMeta):
    pass
