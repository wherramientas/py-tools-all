from .autoEnum import enum, Enum

__all__ = ('enum', 'Enum')
