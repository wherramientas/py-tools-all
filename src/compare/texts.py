__all__ = ['similarity']

from typing import Literal


def _count_chars(str1, str2, ignore_mayus=False) -> dict:
    """
    Args:
        str1: The first string to count characters from.
        str2: The second string to search for characters from str1.
        ignore_mayus: Flag to ignore case sensitivity. Default is False.

    Returns:
        A dictionary containing the count of each character from str1 in str2.

    Example:
        >>> _count_chars('abc', 'abbccc')
        {'a': 1, 'b': 2, 'c': 3}
    """
    if ignore_mayus:
        str1 = str1.lower().replace(' ', '')
        str2 = str2.lower().replace(' ', '')

    counts = {}
    for char in str1:
        counts[char] = str2.count(char)
    return counts


def _count_words(str1, str2, ignore_mayus=False) -> dict:
    if ignore_mayus:
        str1 = str1.lower()
        str2 = str2.lower()

    counts = {}
    for word in str1.split(' '):
        counts[word] = str2.count(word)
    return counts


def _clean_text(str1, method: Literal['words', 'character'] = 'character') -> set:
    """
    Args:
        str1 (str): The input text to be cleaned.

    Returns:
        set: A set of characters without spaces.

    """
    if method == 'character':
        return set(str1.replace(' ', ''))

    if method == 'words':
        return set(str1.split(' '))


def similarity(str1, str2, ignore_mayus=True, method: Literal['character', 'words'] = 'character'):
    """
    Calculate the similarity between two strings based on character or word matching.

    Args:
        str1: The first string for comparison.
        str2: The second string for comparison.
        ignore_mayus: A boolean value indicating whether to ignore capitalization in the comparison. Defaults to True.
        method: A string literal indicating the matching method to use. Valid values are 'character' (default) or 'words'.

    Returns:
        A float representing the similarity between the two strings as a percentage.

    Example usage:
        str1 = 'Hello'
        str2 = 'hello'
        result = similarity(str1, str2, ignore_mayus=True, method='character')
        print(result) # Outputs: 83.333
    """
    if method == 'character':
        resp = _count_chars(str1, str2, ignore_mayus)
        sum_res = sum([1 for i in resp.values() if i != 0])
        return round((sum_res / len(_clean_text(str1, method))) * 100, 3)

    if method == 'words':
        resp = _count_words(str1, str2, ignore_mayus)
        print(resp)
        sum_res = sum([1 for i in resp.values() if i != 0])
        print(sum_res)
        return round((sum_res / len(_clean_text(str1, method))) * 100, 3)

