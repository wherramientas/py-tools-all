def order_extend(*args):
    """
    Ordena una serie de datos que pueden ser enteros, flotantes, listas, tuplas o listas de listas.

    Args:
    - *args: Datos a ordenar.

    Returns:
    - List: Lista ordenada.

    Example:
    >>> order_extend(3, 1, 4, [5, 2], (6, 0), [9, 8, [7]])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    """
    datos = []

    for elemento in args:
        if isinstance(elemento, (int, float)):
            datos.append(elemento)
        elif isinstance(elemento, (list, tuple)):
            datos.extend(_flatten(elemento))
        else:
            raise ValueError(f'Value {elemento} is not supported. Type: {type(elemento)}')

    return sorted(datos)


def _flatten(lst):
    """
    Función auxiliar para aplanar listas y tuplas anidadas.

    Args:
    - lst: Lista o tupla para aplanar.

    Returns:
    - List: Lista aplanada.
    """
    resultado = []
    for elemento in lst:
        if isinstance(elemento, (list, tuple)):
            resultado.extend(_flatten(elemento))
        else:
            resultado.append(elemento)
    return resultado
