def get_longest(matriz: list, n_max=0):
    # Initialize the longest string as an empty string

    if isinstance(matriz, list):
        # Iterate over each list in the list_of_lists
        for element in matriz:
            if isinstance(element, list):
                # Cuando encontramos otra lista, recurrimos a esa lista
                n_max = get_longest(element, n_max)
            else:
                # Comparamos la longitud de la cadena del elemento actual con n_max
                n_max = max(n_max, len(str(element)))
    return n_max


def tabulate(matriz: list = None, header: set | list | tuple = None, **kwargs):
    assert matriz is not None, "Param (matriz) is required"
    assert (isinstance(header, set)
            or isinstance(header, list)
            or isinstance(header, tuple)
            or header is None), "Param (header) is required if header is set, tuple or list"

    # Obtener las dimensiones de la matriz
    filas = len(matriz)
    columnas = len(matriz[0])

    if header is not None:
        assert columnas == len(header), "El numero de columnas no es igual al numero de cabeceras"

    # obtener la longitud de la cadena de datos más larga
    long_col = get_longest(matriz)

    if kwargs.get('first_row_header', False):
        # Imprimir la matriz con formato
        for i in range(filas):
            # Imprimir la línea superior de cada fila
            for j in range(columnas):
                exec(f"""print(f"| {{matriz[i][j]:^{long_col}}} ", end='')""")
            print("|")  # Agregar una barra vertical al final de la fila

            if i < filas - 1:
                for j in range(columnas):
                    exec(f'''print(f"|{{'-'*{long_col + 2}:^{long_col}}}", end='')''')
                print("|")


if __name__ == '__main__':
    # Ejemplo de uso
    matriz_ejemplo = [
                      [10000, 20000, 30000],
                      [40, 555555550, 60],
                      [70, 80, 90]]

    tabulate(matriz_ejemplo, header={'Col 1', 'Col 2', 'Col 3'}, first_row_header=True)
