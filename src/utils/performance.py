from time import sleep, time


def delay(funcion):
    """
    Decorador que mide el tiempo de ejecución de una función.

    Args:
    - funcion: La función a decorar.

    Returns:
    - Función decorada.

    Example:
    >>> @delay
    ... def funcion_a_medir():
    ...     sleep(2)
    ...
    >>> funcion_a_medir()
    Tiempo de ejecución: 2.0 segundos
    """

    def wrapper(*args, **kwargs):
        inicio_tiempo = time()
        resultado = funcion(*args, **kwargs)
        fin_tiempo = time()

        tiempo_ejecucion = fin_tiempo - inicio_tiempo
        print(f"Tiempo de ejecución: {tiempo_ejecucion:.10f} segundos")
        return resultado

    return wrapper

