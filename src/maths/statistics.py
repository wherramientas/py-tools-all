import math
import random

from collections import Counter

import numpy as np

from src.maths.aritmetics import prom, sum
from src.utils.order import order_extend, _flatten


def media(*args):
    """
    La media aritmética es la suma de todos los valores dividida por el número
    :return:
    """
    return prom(*args)


def mediana(*args):
    """
    Calcula la mediana de un arreglo ordenado.

    Args:
    - arreglo (List): Arreglo ordenado.

    Returns:
    - float: Mediana del arreglo.

    Example:
    >>> mediana([1, 2, 3, 4, 5])
    3.0
    >>> mediana([1, 2, 3, 4, 5, 6])
    3.5
    """
    lst = order_extend(*args)
    n = len(lst)

    if n % 2 == 0:
        # Si la longitud del arreglo es par, se toma el promedio de los dos valores centrales.
        indice_1 = n // 2
        indice_2 = indice_1 - 1
        return (lst[indice_1] + lst[indice_2]) / 2
    else:
        # Si la longitud del arreglo es impar, se toma el valor central.
        indice_central = n // 2
        return lst[indice_central]


def moda(*args):
    """
    Calcula la moda de un conjunto de datos.

    Args:
    - datos (List): Conjunto de datos.

    Returns:
    - List: Lista de modas (puede contener más de un valor).

    Example:
    >>> moda([1, 2, 3, 3, 4, 4, 5])
    [3, 4]
    >>> moda([1, 2, 3, 4, 5])
    [1, 2, 3, 4, 5]
    """
    conteo = Counter(order_extend(*args))
    frecuencia_maxima = max(conteo.values())

    modas = [valor for valor, frecuencia in conteo.items() if frecuencia == frecuencia_maxima]

    return modas


def varianza(*args, decimal=2):
    """
    Calcula la varianza de una lista de datos.

    Args:
    - datos (List): Lista de datos.

    Returns:
    - float: Varianza de los datos.

    Example:
    >>> varianza([1, 2, 3, 4, 5])
    2.5
    >>> varianza([1, 2, 3, 4, 5, 6])
    2.9166666666666665
    """
    lst = order_extend(*args)
    n = len(lst)
    med = sum(args) / n

    if n < 2:
        raise ValueError("Se requieren al menos dos datos para calcular la varianza.")

    var = sum([(x - med) ** 2 for x in lst])

    return round(var / n, decimal)


def desviacion(*args):
    """
    Calcula la desviación estándar de un conjunto de datos.

    Args:
    - datos (List): Conjunto de datos.

    Returns:
    - float: Desviación estándar de los datos.

    Example:
    >>> desviacion([1, 2, 3, 4, 5])
    1.5811388300841898
    >>> desviacion([1, 2, 3, 4, 5, 6])
    1.8708286933869707
    """
    return math.sqrt(varianza(*args))


def porcentiles(*args, percentil):
    """
    Calcula el percentil de un conjunto de datos.

    Args:
    - datos (List): Conjunto de datos.
    - percentil (float): Valor entre 0 y 100 que representa el percentil deseado.

    Returns:
    - float: Valor correspondiente al percentil.

    Example:
    >>> porcentiles([1, 2, 3, 4, 5], 50)
    3.0
    >>> porcentiles([1, 2, 3, 4, 5, 6], 75)
    4.75
    """
    if not 0 <= percentil <= 100:
        raise ValueError("El percentil debe estar en el rango de 0 a 100.")
    datos = _flatten(*args)
    return np.percentile(datos, percentil)


if __name__ == '__main__':
    l = [random.randint(1, 101)
         if i % 10 != 0
         else [random.randint(1, 101)
               for i in range(10)]
         for i in range(1000)]

    print(porcentiles([0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 4, 4, 4, 4, 7, 8, 9, 9], percentil=90))


def coeficiente_correlacion():
    """
    Mide la fuerza y dirección de la relación lineal entre dos variables.
    :return:
    """
    pass


def regresion_lineal():
    """
    Modela la relación lineal entre dos variables, permitiendo predecir el valor de una variable en función de la otra.
    :return:
    """
    pass


def probabilidad():
    """
    En estadística, la probabilidad se utiliza para medir la certeza o la posibilidad de que ocurra un evento.
    :return:
    """
    pass


def intervalo_confianza():
    """
    Un intervalo que proporciona un rango plausible de valores para un parámetro estadístico.
    :return:
    """
    pass


def prueba_hipotesis():
    """
    Se utilizan para tomar decisiones sobre afirmaciones basadas en datos observados.
    :return:
    """
    pass


def analisis_regresion():
    """
    Explora la relación entre una variable dependiente y una o más variables independientes.
    :return:
    """
    pass
