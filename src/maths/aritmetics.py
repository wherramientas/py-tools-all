"""
Módulo de Aritmética

Este módulo proporciona funciones para realizar operaciones aritméticas, como suma y resta,
con soporte para números, listas y tuplas.

Funciones:
- `sum(*args, errors=False)`: Realiza la suma de una serie de números, incluyendo números en listas o tuplas.
- `sub(*args, errors=False)`: Realiza la resta de una serie de números, incluyendo números en listas o tuplas.
- `prom(*args, decimal: int = 2, errors: bool = False)`: Calcula el promedio de una serie de números,
  incluyendo números en listas o tuplas.

Autor: Walter Cun B
Fecha de Creación: 27/11/2023
"""

from typing import Union

__all__ = ['sum', 'sub', 'prom']


def sum(*args: object, errors: object = False) -> Union[int, float]:
    _sum = 0

    for elemento in args:
        if isinstance(elemento, (int, float)):
            _sum += elemento
        elif isinstance(elemento, (list, tuple)):
            _sum += sum(*elemento)
        elif errors:
            raise ValueError(f'Value {elemento} is not soport is type {type(elemento)}')
    return _sum


def sub(*args, errors=False) -> Union[int, float]:
    """
        Realiza una resta de una serie de números, incluyendo números en listas o tuplas.

        Args:
        - *args: Números o listas/tuplas que contienen números para realizar la resta.
        - errors (bool): Si es True, lanza un ValueError si encuentra un tipo no soportado (por defecto, False).

        Returns:
        - float: El resultado de la resta.

        Raises:
        - ValueError: Si no se proporciona al menos un argumento o si el primer argumento es una lista o tupla.

        Example:
        >>> sub(10, 6)
        4
        >>> sub(10, 6, errors=True)
        ValueError: Value [10, 6] is not supported. Type: <class 'list'>
        >>> sub([10, 6])
        ValueError: El primer argumento debe ser un numero INT o FLOAT
    """
    if not args:
        raise ValueError("La función sub() requiere al menos un argumento.")
    if isinstance(args[0], (list, tuple)):
        raise ValueError("El primer argumento debe ser un numero INT o FLOAT")
    _sub = args[0]

    for elemento in args[1:]:
        if isinstance(elemento, (int, float)):
            _sub -= elemento
        elif isinstance(elemento, (list, tuple)):
            _sub -= sub(*elemento)
        elif errors:
            raise ValueError(f'Value {elemento} is not supported. Type: {type(elemento)}')

    return _sub


def prom(*args, decimal: int = 2, errors: bool = False) -> float:
    """
        Calcula el promedio de una serie de números, incluyendo números en listas o tuplas.

        Args:
        - *args: Números o listas/tuplas que contienen números para calcular el promedio.
        - decimal (int): Número de decimales a redondear en el resultado del promedio (por defecto, 2).
        - errors (bool): Si es True, lanza un ValueError si encuentra un tipo no soportado (por defecto, False).

        Returns:
        - float: El promedio calculado.

        Example:
        >>> prom(1, 2, 3)
        2.0
        >>> prom([1, 2, 3], (4, 5.5))
        3.38
        >>> prom(1, 'a', errors=True)
        ValueError: Value a is not supported. Type: <class 'str'>
    """
    _count = 0
    _sum = 0

    for elemento in args:
        if isinstance(elemento, (int, float)):
            _sum += elemento
            _count += 1
        elif isinstance(elemento, (list, tuple)):
            _sum += sum(*elemento)
            _count += len(elemento)
        elif errors:
            raise ValueError(f'Value {elemento} is not soport is type {type(elemento)}')
    return round(_sum / _count, decimal)
